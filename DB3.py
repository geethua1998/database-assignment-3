import sqlite3
conn = sqlite3.connect('database.sqlite')
c = conn.cursor()

c.execute("SELECT HomeTeam,FTHG,FTAG FROM Matches WHERE HomeTeam='Aachen' AND Season='2010' order by FTHG DESC")
print(c.fetchall())

c.execute("SELECT COUNT(DISTINCT HomeTeam) FROM Matches WHERE Season='2016' AND FTR='H' ORDER BY FTHG DESC")
print(c.fetchall())

c.execute("SELECT * FROM Unique_Teams LIMIT 10")
print(c.fetchall())

c.execute("SELECT Teams_in_Matches.Match_ID,Teams_in_Matches.Unique_Team_ID,Unique_Teams.TeamName FROM Teams_in_Matches ,Unique_Teams  WHERE Teams_in_Matches.Unique_Team_ID=Unique_Teams.Unique_Team_ID")
print(c.fetchall())
c.execute("SELECT Teams_in_Matches.Match_ID,Teams_in_Matches.Unique_Team_ID,Unique_Teams.TeamName FROM Teams_in_Matches JOIN Unique_Teams ON Teams_in_Matches.Unique_Team_ID=Unique_Teams.Unique_Team_ID")
print(c.fetchall())

c.execute("SELECT * FROM Unique_Teams U JOIN Teams T ON U.TeamName=T.teamname LIMIT 10")
print(c.fetchall())

c.execute("SELECT U.Unique_Team_ID,U.TeamName,T.AvgAgeHome,T.Season,T.ForeignPlayersHome FROM Unique_Teams U JOIN Teams T ON U.TeamName=T.teamname LIMIT 5")
print(c.fetchall())

c.execute("SELECT MAX(Match_ID),T.Unique_Team_ID,TeamName FROM Teams_in_Matches T join Unique_Teams U ON T.Unique_Team_ID=U.Unique_Team_ID WHERE (TeamName LIKE '%y') OR (TeamName like '%r') GROUP BY T.Unique_Team_ID,TeamName")
print(c.fetchall())

conn.commit()
conn.close()