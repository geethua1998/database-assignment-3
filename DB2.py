import sqlite3
conn = sqlite3.connect('database.sqlite')
c = conn.cursor()

c.execute("SELECT COUNT(*) FROM Teams")
print(c.fetchall())

c.execute("SELECT DISTINCT Season FROM Teams")
print(c.fetchall())

c.execute("SELECT MIN(StadiumCapacity) As SmallestStadium,MAX(StadiumCapacity) AS LargestStadium FROM Teams ")
print(c.fetchall())

c.execute("SELECT SUM(KaderHome) FROM Teams where Season='2014'")
print(c.fetchall())

c.execute("SELECT AVG(FTHG) FROM Matches WHERE HomeTeam='Man United' ")
print(c.fetchall())

conn.commit()
conn.close()